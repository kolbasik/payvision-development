# Details

## Launch

```bash
npm install --only=dev
npm start
```

## Trace

Please, open the DevTool on the page in the browser and execute the following code in Console tab to enable the trace information and do not forget to refresh the page:

```js
window.sessionStorage.__RIOT_DEVTOOLS__ = true
```

## Frameworks and libraries

The main goal of this solution was to try as much as posible the letest HTML5 and CSS3 features and [Riot.js](http://riotjs.com/) javascript framework because it's most similar to Web Components.

The list of used CSS3 features:

- [CSS Variables](https://caniuse.com/#feat=css-variables)
- [CSS Flexible Box Layout](https://caniuse.com/#search=flex)
- [CSS3 Media Queries](https://caniuse.com/#search=CSS3%20Media%20Queries)
- [@font-face Web fonts](https://caniuse.com/#search=Web%20fonts)

The list of used javascript features:

- [JavaScript modules via script tag](https://caniuse.com/#search=modules)
- [Async functions](https://caniuse.com/#search=Async%20functions)
- [Arrow functions](https://caniuse.com/#search=arrow%20functions)

TODO list:

- Cover the components with tests
- Error Handling + toastr for non-blocking notifications
- Credentials is hard coded instead of requesting on startup
- Build a proper combobox according to requirements instead of using a select tag
- Think of a better way to show the table information on small screens
- Use [RIOT Route](http://riotjs.com/api/route/) to support navigation between pages

# Frontend Software Engineer - Rendering transactions

As a payment gateway, Payvision offers multiple APIs to process payments and credits (also known as transactions). In this code challenge, you will be in charge of creating a stunning view for **rendering transactions**. Use the technology and/or frameworks that make you feel comfortable. Don't hesitate to contact us if you have any questions regarding this challenge.

## Challenge details

Here you have the requirements for the challenge:
1. Create a web project where the user consume the **transactions endpoint**.
2. The app should render the information from the **transactions endpoint** described below.
3. Your code should be maintainable and extensible as possible.
4. Here you have the design that you have to apply. See our [zeplin page!](https://scene.zeplin.io/project/5aba58ec2ad5c9a98d97c76e)
5. Responsive is mandatory.
6. Use the technology that you consider more useful.
7. Create a readme with the explanation of how to start the project.
8. Package the solution and send to jobs.spain@payvision.com

Please, find bellow all the information of the endopoint that you should use.

## Transactions endpoint

```
https://jovs5zmau3.execute-api.eu-west-1.amazonaws.com/prod/transactions
```

This is the endpoint for retrieving transactions from the API. This is a protected endpoint; provide the following credentials using the **basic auth** mechanism.

```
Username: code-challenge
Password: payvisioner
```

This endpoint should be called with `GET` and accept filters.

| Filter name   |   Possible values |
| ---           |   ---             |
| `action`      | `payment`, `credit`
| `currencyCode` | `EUR`, `JPY`, `USD` |
| `orderBy`     | `date`, `-date`   |

Here you have some examples:
```
https://jovs5zmau3.execute-api.eu-west-1.amazonaws.com/prod/transactions?currency=EUR
https://jovs5zmau3.execute-api.eu-west-1.amazonaws.com/prod/transactions?action=refund
https://jovs5zmau3.execute-api.eu-west-1.amazonaws.com/prod/transactions?action=charge&currency=USD
```

That's all you need to create a simple dashboard to list the transactions. There are no restrictions about the styling or extra functionalities, be creative!
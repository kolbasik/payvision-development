if (window.__RIOT_DEVTOOLS__ = Boolean(window.sessionStorage.__RIOT_DEVTOOLS__)) {
  riot.mixin({
    init: function() {
      this.on('*', (event, ...args) => 
        console.log(`${this.__.impl.name}:${this._riot_id}:${event}`, ...args, this))
    }
  })
}

riot.mixin('events', {
  init: function(opts) {
    const handle = function(name, ...args) {
      const callback = opts.events && opts.events[name]
      callback && callback(...args)
    }
    this.on('mount', () => this.on('*', handle))
    this.on('unmount', () => this.off('*', handle))
  }
})
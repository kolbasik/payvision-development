const api = {
  baseUrl: 'https://jovs5zmau3.execute-api.eu-west-1.amazonaws.com/prod',
  credentials: 'Basic ' + btoa('code-challenge:payvisioner'),

  encode: function(options) {
    return Object.entries(options)
      .filter(([k,v]) => !!v)
      .map(([k,v]) => encodeURIComponent(k) + '=' + encodeURIComponent(v))
      .join('&')
  },

  transactions: function(options) {
    return fetch(`${this.baseUrl}/transactions?${this.encode(options)}`, {
      headers: new Headers({
        'Authorization': this.credentials,
        'Accept': 'application/json'
      })
    }).then(res => { 
      return res.json().then(json => {
        if (res.ok) return json
        throw new Error(json.message);
      })
    })
  }
}

export default api